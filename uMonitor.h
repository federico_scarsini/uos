
typedef enum
{
	FunctId_Thread_01,
	FunctId_Thread_02,
	
	FunctId_uOS_Init,
 	FunctId_uOS_Start,
 	FunctId_uOS_Stop,
 	FunctId_uOS_ThreadNew,
	
	MAXFUNCTID,
	
} uMonitorFunctionId_t;
	
void uMonitor_RegisterFunction(uMonitorFunctionId_t funcId);
void uMonitor_TimerStart(uMonitorFunctionId_t funcId);
void uMonitor_TimerStop(uMonitorFunctionId_t funcId);
