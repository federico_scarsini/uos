#include "utils.h"

typedef struct {
  uint32_t      id;
  uint32_t      period;
  uint32_t      timeout;
  funcPointer_t func;
} uOS_Thread_t;


void 		 uOS_Init(uint32_t tickPeriod_ms);
void 		 uOS_Start(void);
void 		 uOS_Stop(void);
void     uOS_Schedule(void);
void 		 uOS_ThreadNew(uint32_t period, funcPointer_t threadHandler);
uint32_t uOS_TicksGet(void);
