//----------------------------------------------------------------------------//
// Includes                                                                   //
//----------------------------------------------------------------------------//
#include "RTE_Components.h"  
#include CMSIS_device_header
#include "uMonitor.h"
#include "uOS.h"

//----------------------------------------------------------------------------//
// Defines                                                                    //
//----------------------------------------------------------------------------//
typedef struct
{
	uMonitorFunctionId_t id;
	uint32_t count;
	uint32_t max_us;
	uint32_t min_us;
	uint32_t avg_us;
	uint32_t start;
	uint32_t stop;
	uint32_t sum;
} uMonitorRecord_t;


//----------------------------------------------------------------------------//
// Private variables                                                          //
//----------------------------------------------------------------------------//
uMonitorRecord_t recordList[MAXFUNCTID];


//----------------------------------------------------------------------------//
// Private functions declaration                                              //
//----------------------------------------------------------------------------//



//----------------------------------------------------------------------------//
// Public functions implementation                                            //
//----------------------------------------------------------------------------//
void uMonitor_RegisterFunction(uMonitorFunctionId_t funcId)
{
	recordList[funcId].id     = funcId;
	recordList[funcId].count  = 0;
	recordList[funcId].max_us = 0;
	recordList[funcId].min_us = 0xFFFFFFFF;
	recordList[funcId].avg_us = 0;
	recordList[funcId].start  = 0;
	recordList[funcId].stop   = 0;
	recordList[funcId].sum    = 0;
}

void uMonitor_TimerStart(uMonitorFunctionId_t funcId)
{
	recordList[funcId].start = (uint32_t)((uOS_TicksGet()*1000) + (SysTick->LOAD - SysTick->VAL));
}

void uMonitor_TimerStop(uMonitorFunctionId_t funcId)
{
	recordList[funcId].stop = (uint32_t)((uOS_TicksGet()*1000) + (SysTick->LOAD - SysTick->VAL));
	recordList[funcId].count++;
	uint32_t curr = recordList[funcId].stop - recordList[funcId].start;
	recordList[funcId].sum += curr;
	recordList[funcId].avg_us = (uint32_t)(recordList[funcId].sum/recordList[funcId].count);

	if(curr > recordList[funcId].max_us)
	{
		recordList[funcId].max_us = curr;
	}
	
	if(curr <= recordList[funcId].min_us)
	{
		recordList[funcId].min_us = curr;
	}

}


//----------------------------------------------------------------------------//
// Private functions implementation                                           //
//----------------------------------------------------------------------------//

