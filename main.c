#include "RTE_Components.h"  
#include CMSIS_device_header
#include "uos.h"

#ifdef DEBUG
#include "uMonitor.h"
#endif

#ifdef SYSTEMVIEW
#include "SEGGER_SYSVIEW.h"
#include "SEGGER_SYSVIEW_Conf.h"
#endif


void t01_Handler(void);
void t02_Handler(void);

void t01_Handler(void)
{
	#ifdef DEBUG
	uMonitor_TimerStart(FunctId_Thread_01);
	#endif
	
  int a[] = {1, 1, 1, 1, 1};
  int b[] = {0, 0, 0, 0, 0};

  for(int i=0; i<5; i++)
  {
    b[i] = a[i]+10;
  }

  for(int i=0; i<5; i++)
  {
    b[i] = b[i]*10;
  }
	
	#ifdef DEBUG
	uMonitor_TimerStop(FunctId_Thread_01);
	#endif
}

void t02_Handler(void)
{
	#ifdef DEBUG
	uMonitor_TimerStart(FunctId_Thread_02);
	#endif
	
  int a[] = {2, 2, 2, 2, 2};
  int b[] = {0, 0, 0, 0, 0};

  for(int i=0; i<5; i++)
  {
    b[i] = a[i]*10;
  }

  for(int i=0; i<5; i++)
  {
    b[i] = b[i]*10;
  }
	
	#ifdef DEBUG
	uMonitor_TimerStop(FunctId_Thread_02);
	#endif
}


int main(void)
{
	SystemCoreClockUpdate();
	
	#ifdef DEBUG
	uMonitor_RegisterFunction(FunctId_uOS_Init);
	uMonitor_RegisterFunction(FunctId_uOS_Start);
 	uMonitor_RegisterFunction(FunctId_uOS_Stop);
 	uMonitor_RegisterFunction(FunctId_uOS_ThreadNew);
	uMonitor_RegisterFunction(FunctId_Thread_01);
	uMonitor_RegisterFunction(FunctId_Thread_02);
	#endif
	
	#ifdef SYSTEMVIEW
  SEGGER_SYSVIEW_Conf();
	#endif
	
	
  uOS_Init(1);
	
	uOS_ThreadNew(10, t01_Handler);
	uOS_ThreadNew( 5, t02_Handler);

  uOS_Start();
	uOS_Schedule();
		
  return 0;
}
