//----------------------------------------------------------------------------//
// Includes                                                                   //
//----------------------------------------------------------------------------//
#include "RTE_Components.h"  
#include CMSIS_device_header
#include "uos.h"


#ifdef SYSTEMVIEW
#include "SEGGER_SYSVIEW.h"
#include "SEGGER_SYSVIEW_Conf.h"
#include <stdio.h> 
#endif


//----------------------------------------------------------------------------//
// Defines                                                                    //
//----------------------------------------------------------------------------//
#define MAX_THREAD_COUNT 10


//----------------------------------------------------------------------------//
// Private variables                                                          //
//----------------------------------------------------------------------------//
uint32_t     cnt;
uint32_t     msTicks; 
uOS_Thread_t threadsList[MAX_THREAD_COUNT];


//----------------------------------------------------------------------------//
// Private functions declaration                                              //
//----------------------------------------------------------------------------//
#ifdef SYSTEMVIEW
void SYSVIEW_AddTask(void* pTask, const char* sName, uint32_t Prio);
#endif

//----------------------------------------------------------------------------//
// Public functions implementation                                            //
//----------------------------------------------------------------------------//
void uOS_Init(uint32_t tickPeriod_ms)
{		
  cnt     = 0;
	msTicks = 0;
	
  uint32_t returnCode = SysTick_Config((SystemCoreClock/1000)*tickPeriod_ms);
  
  if (returnCode != 0)  {
		while(1);
    // Error Handling 
  }
}

void uOS_Start(void)
{
  __enable_irq();
}

void uOS_Schedule(void)
{
	while(1)
	{
		for(uint32_t i=0; i<cnt; i++)
		{
			threadsList[i].timeout--;

			if(threadsList[i].timeout == 0)
			{ 
				threadsList[i].timeout = threadsList[i].period;
				
				#ifdef SYSTEMVIEW
				SEGGER_SYSVIEW_OnTaskStartExec((uint32_t)threadsList[i].func);
				#endif
				
				threadsList[i].func();
				
				#ifdef SYSTEMVIEW
				SEGGER_SYSVIEW_OnTaskTerminate((uint32_t)threadsList[i].func);
				#endif
			}
		}
		__wfi();
  }
}


void uOS_Stop(void)
{
  __disable_irq();
}


void uOS_ThreadNew(uint32_t period, funcPointer_t threadHandler)
{
  threadsList[cnt].id 		 = cnt;
  threadsList[cnt].period	 = period;
  threadsList[cnt].timeout = period ;
  threadsList[cnt].func    = threadHandler;
	
	#ifdef SYSTEMVIEW
	char str[12];
	sprintf(str, "Thread %d", cnt);
	SYSVIEW_AddTask(threadHandler, str, 1);
	#endif
	
	cnt++;
}

uint32_t uOS_TicksGet(void)
{
	return msTicks;
}


//----------------------------------------------------------------------------//
// Private functions implementation                                           //
//----------------------------------------------------------------------------//

void SysTick_Handler()
{
	#ifdef SYSTEMVIEW
	SEGGER_SYSVIEW_RecordEnterISR();
	#endif
	
	msTicks++;
	
	#ifdef SYSTEMVIEW
	SEGGER_SYSVIEW_RecordExitISR();
	#endif
}